# Directory to download installer to
$dir = "c:\temp"

#Creating Directory if it does not exist
if(!(test-path $dir)){
    mkdir $dir
}

#Creating a webclient and downloading the file
$webClient = New-Object System.Net.WebClient
$url = "https://go.microsoft.com/fwlink/?LinkID=799445"
$file = "$($dir)\Win10Upgrade.exe"
$webClient.DownloadFile($url,$file)

#Starting the upgrade process. 
Start-Process -FilePath $file -ArgumentList "/quietinstall /skipeula /auto upgrade /copylogs $dir" -verb runas