# Upgrade-Windows

Script to upgrade from Windows 7 to Windows 10 without having to do it manually.
To start the script either:

## manually
  1. Copy the code from "Upgrade-Windows.ps1"
  2. Start PowerShell as Administrator
  3. Paste the code.
  4. Installation will Start

## Automatically
  1. Download the powershell script.
  2. Start PowerShell as Administrator
  3. Run the following code:  
    ```(Invoke-WebRequest -Uri "https://gitlab.com/Hollanderrr/upgrade-windows/raw/master/Upgrade-Windows.ps1").Content | Invoke-Expression
    ```
  4. Installation will start.
